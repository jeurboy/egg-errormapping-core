# Error Mapping Core
This is core class for error mapping message

### Installation
Require this package in your `composer.json` and update composer. This will download the package.
```php
{
    "require": {
        "eggdigital/egg-errormapping-core": "dev-master"
    },
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "eggdigital/egg-errormapping-core",
                "version": "dev-master",
                "source": {
                    "url": "https://bitbucket.org/jeurboy/egg-errormapping-core.git",
                    "type": "git",
                    "reference": "origin/master"
                },
                "autoload": {
                    "classmap": [
                        "src/classes"
                    ],
                    "psr-0": {
                        "EggDigital\\ErrorMapping\\": "src/"
                    }
                }
            }
        }
    ]
}
```

### Usage
You can copy this code or copy file `cli.php` in folder `template`
```php
// cli.php
<?php

// Import core error mapping
use EggDigital\ErrorMapping\Classes\BaseErrorMapping;

// Build object BaseErrorMapping
$msg = new BaseErrorMapping();

// Printing result of message (Pass params error_code and language)
print_r($msg->getErrorMessage('400', 'th'));
```
