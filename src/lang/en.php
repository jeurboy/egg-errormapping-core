<?php

$messages = [
    '400' => [
        'code'    => '400',
        'message' => 'Bad Request'
    ],
    '401' => [
        'code'    => '401',
        'message' => 'Unauthorized'
    ],
    '402' => [
        'code'    => '402',
        'message' => 'Payment Required'
    ],
    '403' => [
        'code'    => '403',
        'message' => 'Forbidden'
    ],
    '404' => [
        'code'    => '404',
        'message' => 'Not Found'
    ],
    '405' => [
        'code'    => '405',
        'message' => 'Method Not Allowed'
    ]
];
