<?php

namespace EggDigital\ErrorMapping\Classes;

class BaseErrorMapping
{
    public function getErrorMessage($code, $lang)
    {
        // Declared language
        $list_lang = ['th', 'en'];

        // Check language exist
        if (!in_array($lang, $list_lang)) {
            echo "{$lang} is no language";

            return false;
        }

        // import message
        include __DIR__ . "/../lang/{$lang}.php";

        // Check code exist
        if (!array_key_exists($code, $messages)) {
            echo "Code {$code} is no message";

            return false;
        }

        // Return result message
        return $messages[$code];
    }
}
